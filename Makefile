# This Makefile provides some boilerplate to run conda-dist,
# test, lint and typechecking targets.
# A docker image is available to perform all CI steps
# locally, in an enviroment that matches Gitlab's CI container.
#
# Example:
#
# $ make docker-conda
# To generate a suitable python3 + miniconda docker image.
#
# $ make env
# pacakges knowledge-gaps in docker and produces a linux x86_64 binary distribution.
#
# $ make env SKIP_DOCKER=true
# Will run the conda-dist command natively.

# Metadata for development cond dist archives.
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
GIT_COMMIT_HASH := $(shell git rev-parse --short=8 HEAD)
# A docker image based atop debian buster and miniconda3.
# It provides the same run-time as Gitlab CI.
DOCKER_IMG := research/miniconda3
DOCKERFILE := Dockerfile
# Generate x86_64 binaries when running docker on non x86 platform (e.g. apple m1).
DOCKER_PLATFORM := linux/amd64
PROJECT_NAME := differential-privacy
# Do not modify PROJECT_VERSION manually. Use bump2version instead.
PROJECT_VERSION := 0.1.0.dev
CONDA_DIST := ./dist/${PROJECT_NAME}-${PROJECT_VERSION}.conda-${GIT_BRANCH}-${GIT_COMMIT_HASH}

# Build an x86_64-linux docker image.
docker-conda:
	docker buildx build --platform ${DOCKER_PLATFORM} -t ${DOCKER_IMG} -f ${DOCKERFILE} .

ifneq ($(SKIP_DOCKER),true)
CURRENT_DIR := $(shell pwd)
DOCKER_CMD := docker run -it \
			  --rm \
			  --platform ${DOCKER_PLATFORM} \
			  -v ${CURRENT_DIR}:/root \
			  -e SKIP_DOCKER=true \
			  -w /root ${DOCKER_IMG}

# The research/miniconda3 image is required to run
# CI targets in a docker container.
env: docker-conda
test: docker-conda
lint: docker-conda
typecheck: docker-conda
endif

# Create a conda dist archive, containing this project and all
# its dependencies.
env:
	${DOCKER_CMD} bash -c "source /opt/conda/etc/profile.d/conda.sh && \
	conda activate && \
	conda-dist --dist-env-prefix=${CONDA_DIST}"

# Run the pytest suite.
test:
	${DOCKER_CMD} bash -c "source /opt/conda/etc/profile.d/conda.sh && \
	conda activate && \
	tox -e py37"

# Run linting with flake8 and mypy.
lint:
	${DOCKER_CMD} bash -c "source /opt/conda/etc/profile.d/conda.sh && \
	conda activate && \
	tox -e lint"

.PHONY: docker-conda env test lint

