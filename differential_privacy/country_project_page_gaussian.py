# system packages
import argparse
from math import sqrt, log as ln

# local metric functions
from differential_privacy.utils.pageview_metrics import calc_error_country_project_page
from differential_privacy.utils.budget import write_to_global_privacy_budget

# pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as sf
import pyspark.sql.types as spty

# tumult analytics
from tmlt.analytics.privacy_budget import RhoZCDPBudget
from tmlt.analytics.query_builder import QueryBuilder
from tmlt.analytics.session import Session
from tmlt.analytics.keyset import KeySet
from tmlt.analytics.protected_change import AddMaxRowsInMaxGroups

# tumult core
from tmlt.core.utils.cleanup import cleanup

#------------ START MAIN SCRIPT ------------#
EPSILON = 1
DELTA = 1e-7
PV_THRESH = 150
CONTRIB_THRESH = 10
RELEASE_THRESH = 90

# all pageviews for a day from pageview_actor
pv_query = """
SELECT
  page_id,
  pageview_info['project'] as project,
  geocoded_data['country'] as country
FROM wmf.pageview_actor pa
JOIN htriedman.non_country_protection_list ncpl
ON pa.geocoded_data['country'] = ncpl.country_name
WHERE
  pa.agent_type = 'user'
  AND pa.x_analytics_map['include_pv'] = 1
  AND pa.namespace_id = 0
  AND pa.year = {year}
  AND pa.month = {month}
  AND pa.day = {day}
"""

# select all unique pages from pageview_actor with more than `count` views
filter_query = """
SELECT
  pageview_info['project'] AS project,
  page_id
FROM
  wmf.pageview_actor
WHERE
  agent_type = 'user'
  AND COALESCE(pageview_info['project'], '') != ''
  AND year = {year}
  AND month = {month}
  AND day = {day}
GROUP BY 1, 2
HAVING count(*) >= {pv_thresh}
"""

# all countries we are releasing data for
country_query = """
SELECT
  country_name as country, country_code
FROM
  htriedman.non_country_protection_list
"""

# all countries, subcontinents, and continents for metrics
geo_metrics_query = """
SELECT
    country,
    subcont_region as subcontinent,
    continent
FROM
    isaacj.country_to_region
"""

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Specify context for differential privacy aggregation of pageview_actor'
    )
    parser.add_argument('year', help='year of aggregation')
    parser.add_argument('month', help='month of aggregation')
    parser.add_argument('day', help='day of aggregation')
    return parser.parse_args()

def gen_rho(epsilon, delta):
    logterm = ln(1 / delta)
    # Theorem 3.5 in https://arxiv.org/pdf/1605.02065.pdf
    # This is not tight, but should be a good enough approximation for experimentation
    return (sqrt(epsilon+logterm) - sqrt(logterm))**2

def run_dp(args, spark, log):
    log.info("querying hive tables...")
    # select all pageviews from a day and drop null values
    df = spark.sql(pv_query.format(year=args.year, month=args.month, day=args.day))
    df = df.dropna()

    # combine all columns into one null-separated column for later aggregation
    rdd = df.rdd.map(lambda r: (f'{r[0]}\0{r[1]}\0{r[2]}',))
    schema = spty.StructType([spty.StructField('page_project_country',
                              spty.StringType(),
                              False)])
    combined_df = spark.createDataFrame(rdd, schema)
    combined_df.cache()
    combined_df.take(1)

    # select all countries we're releasing data for
    countries = spark.sql(country_query)
    countries.cache()
    countries.take(1)

    country_df = countries.select("country")

    # get geo regions for metrics
    geo_metrics_df = spark.sql(geo_metrics_query)

    # create article
    article_df = spark.sql(
        filter_query.format(year=args.year,
                            month=args.month,
                            day=args.day,
                            pv_thresh=PV_THRESH)
    )
    article_df.cache()
    article_df.take(1)

    log.info("creating keyspace")
    # cross join countries and articles to get keyspace and cache
    key_df = country_df.crossJoin(article_df)
    key_df = key_df.dropna()
    key_df.take(1)

    # combine all columns into one null-separated column that matches combined_df above
    key_rdd = key_df.rdd.map(lambda r: (f'{r[2]}\0{r[1]}\0{r[0]}',))
    key_schema = spty.StructType([spty.StructField('page_project_country',
                                  spty.StringType(),
                                  False)])
    combined_key_df = spark.createDataFrame(key_rdd, key_schema)
    combined_key_df.cache()
    combined_key_df.take(1)
    ks = KeySet.from_dataframe(combined_key_df)

    log.info("running private aggregation")
    session = Session.from_dataframe(
        privacy_budget=RhoZCDPBudget(gen_rho(epsilon=EPSILON, delta=DELTA)),
        source_id="combined_pageview",
        dataframe=combined_df,
        protected_change=AddMaxRowsInMaxGroups(grouping_column='page_project_country',
                                               max_groups=10,
                                               max_rows_per_group=1)
    )

    query = (
        QueryBuilder("combined_pageview")
        .groupby(ks)
        .count(name="gbc")
    )

    private = session.evaluate(query, RhoZCDPBudget(gen_rho(epsilon=EPSILON, delta=DELTA)))

    # split null-separated column into multiple columns for later error calculations
    split_col = sf.split(private['page_project_country'], '\0')
    private = private.withColumn('page_id', split_col.getItem(0))
    private = private.withColumn('project', split_col.getItem(1))
    private = private.withColumn('country', split_col.getItem(2))
    private = private.select(['country', 'project', 'page_id', 'gbc'])

    log.info('running nonprivate aggregation...')
    nonprivate = df.groupby('country', 'project', 'page_id').count()

    log.info('joining tables for error calculations...')
    private_rounded = (
        private.withColumn("gbc", sf.when(sf.col("gbc") < 0, 0).otherwise(sf.col("gbc")))
    )
    private_rounded_geo = private_rounded.join(geo_metrics_df, on=['country'])
    joined = (
        nonprivate.join(private_rounded_geo, ['country', 'project', 'page_id'], how='outer')
        .na.fill({'count': 0, 'gbc': 0})
    )

    log.info('conducting error calculations...')
    calc_error_country_project_page(
        df=joined,
        spark=spark,
        year=args.year,
        month=args.month,
        day=args.day
    )

    log.info(f"filtering output to only counts > {RELEASE_THRESH}...")
    # filter to just entries above threshold (90)
    private = private.filter(f"gbc >= {RELEASE_THRESH}")

    log.info("saving final table...")
    # save output
    spark.sql("SET hive.exec.dynamic.partition=true")
    spark.sql("SET hive.exec.dynamic.partition.mode=nonstrict")

    private = (
        private.join(countries, on="country")
        .withColumn("private_count", private.gbc.cast(spty.IntegerType()))
        .withColumn("epsilon", sf.lit(EPSILON))
        .withColumn("delta", sf.lit(DELTA))
        .withColumn("noise_type", sf.lit("gaussian"))
        .withColumn("noise_scale", sf.lit(10))
        .withColumn("release_threshold", sf.lit(RELEASE_THRESH))
        .withColumn("year", sf.lit(int(args.year)))
        .withColumn("month", sf.lit(int(args.month)))
        .withColumn("day", sf.lit(int(args.day)))
    )
    
    private = private.select([
        'country',
        'country_code',
        'project',
        'page_id',
        'private_count',
        'epsilon',
        'delta',
        'noise_type',
        'noise_scale',
        'release_threshold',
        'year',
        'month',
        'day'
    ])
    
    (
        private.write.mode("append")
        .insertInto("differential_privacy.country_project_page")
    )

    # write to privacy register
    log.info("writing to global privacy register...")
    write_to_global_privacy_budget(
        spark=spark,
        table="wmf.pageview_actor",
        epsilon=EPSILON,
        delta=DELTA,
        rho=gen_rho(epsilon=EPSILON, delta=DELTA),
        year=args.year,
        month=args.month,
        day=args.day
    )

#------------- END MAIN SCRIPT -------------#

def main():
    # parse args
    args = parse_args()

    # get spark session and run main
    spark = SparkSession.builder.getOrCreate()
    # TODO: investigate if there's a better way to do this
    log = spark.sparkContext._jvm.org.apache.log4j.LogManager.getLogger(__name__)

    run_dp(args=args, spark=spark, log=log)

    # cleanup tumult analytics and spark cluster
    log.info("cleaning up tmlt.analytics and stopping spark...")
    cleanup()
    spark.sparkContext.stop()
    spark.stop()
    log.info("done")

if __name__ == "__main__":
    main()
