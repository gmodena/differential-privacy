# helper functions, constants, and classes
from differential_privacy.utils.editor_shared import (
    parse_args,
    keyset_query,
    labels,
    GeoEditorsArgs,
    do_dp_aggregation
)

# pyspark
from pyspark.sql import SparkSession

# tumult core
from tmlt.core.utils.cleanup import cleanup

#------------ JOB-SPECIFIC SQL QUERIES ------------#
# all edits for a month from editors_daily
edit_query = """
SELECT
  wiki_db,
  country_code,
  user_fingerprint_or_name as user,
  CAST(SUM(edit_count) AS FLOAT) as sum_edits,
  month
FROM
  wmf.editors_daily
WHERE
  month = '{month}'
  -- Filter out bot actions and non-edit actions
  AND size(user_is_bot_by) = 0
  AND action_type IN (0, 1)
GROUP BY
  wiki_db,
  country_code,
  user_fingerprint_or_name,
  month
"""

if __name__ == "__main__":
    meta_args = parse_args()

    # get spark cluster and logger
    spark = SparkSession.builder.getOrCreate()
    spark.sql("SET hive.exec.dynamic.partition=true")
    spark.sql("SET hive.exec.dynamic.partition.mode=nonstrict")

    log = spark.sparkContext._jvm.org.apache.log4j.LogManager.getLogger(__name__)

    for drc in ['Lower risk', 'Medium risk', 'Higher risk']:
        # create global keyset
        log.info(f'creating {drc} keyset and setting constants...')
        bin_df = spark.createDataFrame([(label,) for label in labels], ["edit_range"])
        keyset_df = spark.sql(keyset_query.format(data_risk_classification=drc))
        keyset_df = keyset_df.crossJoin(bin_df)
        
        args = GeoEditorsArgs(meta_args.time_start, meta_args.output_table)
        # get raw data
        log.info(f'doing {drc} source data query...')
        df = spark.sql(edit_query.format(month=args.time_start))
        df = df.dropna()
        # run aggregation
        private, errs = do_dp_aggregation(
            spark=spark,
            df=df,
            keyset_df=keyset_df,
            data_risk_classification=drc,
            time_frame="month",
            time_start=args.time_start,
            log=log
        )

        log.info('writing output to hive...')
        # write output to hive
        private.write.mode("append").insertInto(f"differential_privacy.{args.output_table}")

        # if this is a prod context, write error and loss to hive as well
        if args.output_table == "geoeditors_monthly":
            log.info('writing error to hive...')
            errs.write.mode("append").insertInto("differential_privacy.geoeditors_monthly_error")

    log.info('cleaning up...')
    cleanup()
    spark.sparkContext.stop()
    spark.stop()
