-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--     year                  -- year of partition to compute statistics for.
--     month                 -- month of partition to compute statistics for.
--     day                   -- day of partition to compute statistics for.
--     hour                  -- hour of partition to compute statistics for.
--
-- Usage:
--     spark3-sql --master yarn -f join_titles.hql                        \
--         -d destination_directory=/wmf/tmp/differential_privacy/example \
--         -d year=2023                                                   \
--         -d month=2                                                     \
--         -d day=10                                                      \
--         -d coalesce_partions=1

SET spark.hadoop.hive.exec.compress.output=false;

WITH titles AS (
    SELECT
        project,
        page_title,
        page_id
    FROM
        wmf.pageview_hourly
    WHERE
        agent_type = 'user'
        AND project is not null
        AND page_title != '-'
        AND year = ${year}
        AND month = ${month}
        AND day = ${day}
    GROUP BY project, page_title, page_id
    HAVING SUM(view_count) >= 150
),

joined AS (
    SELECT
        dp.country as country,
        dp.country_code as country_code,
        dp.project as project,
        dp.page_id as page_id,
        dp.private_count as gbc,
        t.page_title as page_title,
        ROW_NUMBER() OVER (
            PARTITION BY dp.country, dp.project, dp.page_id
            ORDER BY t.page_title ASC
        ) AS row_num
    FROM differential_privacy.country_project_page dp
    JOIN titles t
    ON
        dp.project = t.project
        AND cast(dp.page_id as int) = t.page_id
    WHERE
        dp.year = ${year}
        AND dp.month = ${month}
        AND dp.day = ${day}
)

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    -- Set 0 as volume column since we don't use it.
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' '\t')

    SELECT /*+ COALESCE(${coalesce_partitions}) */
        country,
        country_code,
        project,
        page_id,
        page_title,
        gbc
    FROM joined
    WHERE row_num = 1
;
