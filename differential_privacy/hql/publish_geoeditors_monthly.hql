-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--     year                  -- year of partition to compute statistics for.
--     month                 -- month of partition to compute statistics for.
--
-- Usage:
--     spark3-sql --master yarn -f publish_geoeditors_monthly.hql         \
--         -d destination_directory=/wmf/tmp/differential_privacy/example \
--         -d month='2023-05'                                             \
--         -d coalesce_partitions=1

SET spark.hadoop.hive.exec.compress.output=false;

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    -- Set 0 as volume column since we don't use it.
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' '\t')

    SELECT /*+ COALESCE(${coalesce_partitions}) */
        gm.wiki_db,
        CONCAT(CONCAT(w.language_code, '.'), w.database_group) as project,
        c.name as country_name,
        gm.country_code,
        gm.edit_range,
        gm.count_eps,
        gm.sum_eps,
        gm.count_release_thresh,
        gm.private_count,
        gm.private_sum,
        gm.month
    FROM differential_privacy.geoeditors_monthly gm
    JOIN canonical_data.countries c 
    ON c.iso_code = gm.country_code
    JOIN canonical_data.wikis w
    ON w.database_code = gm.wiki_db
    WHERE month = '${month}'
;

